//
//  CameraEnums.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 20/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

public enum CameraSessionPreset {
    case Photo
    case High
    case Medium
    case Low
    case Res352x288
    case Res640x480
    case Res1280x720
    case Res1920x1080
    case Frame960x540
    case Frame1280x720
    case InputPriority
    
    public func foundationPreset() -> String {
        switch self {
        case .Photo: return AVCaptureSessionPresetPhoto
        case .High: return AVCaptureSessionPresetHigh
        case .Medium: return AVCaptureSessionPresetMedium
        case .Low: return AVCaptureSessionPresetLow
        case .Res352x288: return AVCaptureSessionPreset352x288
        case .Res640x480: return AVCaptureSessionPreset640x480
        case .Res1280x720: return AVCaptureSessionPreset1280x720
        case .Res1920x1080: return AVCaptureSessionPreset1920x1080
        case .Frame960x540: return AVCaptureSessionPresetiFrame960x540
        case .Frame1280x720: return AVCaptureSessionPresetiFrame1280x720
        default: return AVCaptureSessionPresetPhoto
        }
    }
    
    public static func availablePresset() -> [CameraSessionPreset] {
        return [
            .Photo,
            .High,
            .Medium,
            .Low,
            .Res352x288,
            .Res640x480,
            .Res1280x720,
            .Res1920x1080,
            .Frame960x540,
            .Frame1280x720,
            .InputPriority
        ]
    }
}


