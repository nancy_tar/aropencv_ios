//
//  BGRAFrame.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 15/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import Foundation

// A helper struct presenting interleaved BGRA image in memory.
struct VideoFrame {
    
    var width:Int
    var height:Int
    var stride:Int
    
    var data:UnsafeMutablePointer<Void>  // void - the underlying data is in the form of a stream of 4x8 bits.(for the color)
}