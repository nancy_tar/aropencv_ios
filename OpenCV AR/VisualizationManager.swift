//
//  VisualizationManager.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 20/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore
import OpenGLES
import GLKit
import CoreMedia
import AVFoundation

// MARK: - Helper structs for readability

struct Vertex {
    
    var Position: (CFloat, CFloat, CFloat)
    var TexCoord: (CFloat, CFloat)
}

var Vertices: (Vertex, Vertex, Vertex, Vertex) = (
    Vertex(Position: (1, -1, 0) , TexCoord: (1, 1)),
    Vertex(Position: (1, 1, 0)  , TexCoord: (1, 0)),
    Vertex(Position: (-1, 1, 0) , TexCoord: (0, 0)),
    Vertex(Position: (-1, -1, 0), TexCoord: (0, 1))
)

var Indices: (GLubyte, GLubyte, GLubyte, GLubyte, GLubyte, GLubyte) = (
    0, 1, 2,
    2, 3, 0
)

// class for rendeging on videoFrame
class VisualizationManager: NSObject {
    
    var eaglLayer: CAEAGLLayer!
    var context: EAGLContext!
    
    var colorRenderBuffer: GLuint = GLuint()
    var positionSlot: GLuint = GLuint()
    var colorSlot: GLuint = GLuint()
    var indexBuffer: GLuint = GLuint()
    var vertexBuffer: GLuint = GLuint()
    var VAO:GLuint = GLuint()
    
}