//
//  VideoFrame.h
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 18/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

#ifndef VideoFrame_h
#define VideoFrame_h


#endif /* VideoFrame_h */


typedef struct Frame{

    /*
     size_t width;
     size_t height;
     size_t bytesPerRow;
     size_t totalBytes;
     unsigned long pixelFormat;
     void *rawPixelData;
     */
    size_t width;
    size_t height;
    size_t bytesPerRow;
    void * rawPixelData;
    
    
    
}VideoFrame;