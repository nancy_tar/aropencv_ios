//
//  VideoSource.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 11/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

//https://github.com/gibachan/OpenCVSample/blob/master/OpenCVSample/CameraUtil.swift
//http://giveitashot.hatenadiary.jp/entry/2014/10/19/190505

import Foundation
import UIKit
import AVFoundation


// This class needed to convert sample buffer frame to UIImage
class CameraUtil {
    
    
    
    // Conversion from sampleBuffer to UIImage
    class func sampleBufferToUIImage(sampleBuffer: CMSampleBufferRef) -> UIImage {
        
        let quartzImage: CGImageRef! = CameraUtil.sampleBufferToCGImage(sampleBuffer);
        
        // Create a UIImage
        let resultImage: UIImage =  UIImage(CGImage: quartzImage)
        return resultImage
    }
    
    /*class func imageBufferToCIImage(imageBuffer: CVImageBufferRef)->CIImage{
        
        let opaqueBuffer = Unmanaged<CVImageBuffer>.passUnretained(imageBuffer).toOpaque()
        let pixelBuffer = Unmanaged<CVPixelBuffer>.fromOpaque(opaqueBuffer).takeUnretainedValue()
        let outputImage = CIImage(CVPixelBuffer: pixelBuffer, options: nil)
    
    }*/
    
    //CGImage
    class func sampleBufferToCGImage(sampleBuffer: CMSampleBufferRef) -> CGImageRef
    {
         // Convert to CVImageBufferRef
        let imageBuffer: CVImageBufferRef! = CMSampleBufferGetImageBuffer(sampleBuffer)
        
        
          // Lock the base address of the image buffer.
        CVPixelBufferLockBaseAddress(imageBuffer, 0)
        
        // Get the base address.
        let baseAddress: UnsafeMutablePointer<Void> = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0)
        // Get bytes per row
        let bytesPerRow: Int = CVPixelBufferGetBytesPerRow(imageBuffer)
        // Get the size
        let width: Int = CVPixelBufferGetWidth(imageBuffer)
        let height: Int = CVPixelBufferGetHeight(imageBuffer)
        
        // Create an RGB color space
        let colorSpace: CGColorSpaceRef! = CGColorSpaceCreateDeviceRGB()
        
        // Create a Bitmap graphic context
        let bitsPerCompornent: Int = 8
        let bitmapInfo = CGBitmapInfo(rawValue: (CGBitmapInfo.ByteOrder32Little.rawValue | CGImageAlphaInfo.PremultipliedFirst.rawValue) as UInt32)
        let newContext: CGContextRef! = CGBitmapContextCreate(baseAddress,
                                                              width,
                                                              height,
                                                              bitsPerCompornent,
                                                              bytesPerRow,
                                                              colorSpace,
                                                              bitmapInfo.rawValue) as CGContextRef!
        
        // Generation of CGImageRef from bitmap graphics context(Quartz image)
        let quartzImage: CGImageRef! = CGBitmapContextCreateImage(newContext!)
        
        
        CVPixelBufferUnlockBaseAddress(imageBuffer, 0)
        return quartzImage;
    }
    
    
    
    //base address is locked outside
    class func imageBufferToVideoFrame(imageBuffer: CVImageBufferRef) -> VideoFrame
    {
        let width: Int = CVPixelBufferGetWidth(imageBuffer)
        let height: Int = CVPixelBufferGetHeight(imageBuffer)
        let bytesPerRow: Int = CVPixelBufferGetBytesPerRow(imageBuffer) // stride
        let baseAddress:UnsafeMutablePointer<Void> = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0)
        let frame = VideoFrame(width:width, height:height, bytesPerRow:bytesPerRow, rawPixelData:baseAddress)
    
        return frame
    }
    
    class func videoFrameToCGImage(frame:VideoFrame)->CGImageRef
    {
        // Create an RGB color space
        let colorSpace: CGColorSpaceRef! = CGColorSpaceCreateDeviceRGB()
        
        // Create a Bitmap graphic context
        let bitsPerCompornent: Int = 8
        let bitmapInfo = CGBitmapInfo(rawValue: (CGBitmapInfo.ByteOrder32Little.rawValue | CGImageAlphaInfo.PremultipliedFirst.rawValue) as UInt32)
        let newContext: CGContextRef! = CGBitmapContextCreate(frame.rawPixelData,
            frame.width,
            frame.height,
            bitsPerCompornent,
            frame.bytesPerRow,
            colorSpace,
            bitmapInfo.rawValue) as CGContextRef!
        
        // Generation of CGImageRef from bitmap graphics context(Quartz image)
        let quartzImage: CGImageRef! = CGBitmapContextCreateImage(newContext!)
    
        return quartzImage;

    }
    

    
}

