//
//  VideoSource.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 18/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import Foundation

@objc
protocol CameraManagerDelegate{
    
    
    //optional func frameReady(frame:VideoFrame)
    optional func frameReady(imageBuffer: CVImageBufferRef)
    
}

let cameraManagerSessionQueueIdentifier = "com.OpenCV-AR.cameramanager"

public class CameraManager: NSObject, AVCaptureVideoDataOutputSampleBufferDelegate{
    
    
    var session = AVCaptureSession()
    var captureDevice = CameraManagerDevice()
    var input : AVCaptureDeviceInput!
    var stillCameraOutput: AVCaptureStillImageOutput!
    var videoOutput: AVCaptureVideoDataOutput!
    
    var delegate:CameraManagerDelegate?
    private var sessionQueue: dispatch_queue_t! = {
        dispatch_queue_create(cameraManagerSessionQueueIdentifier, DISPATCH_QUEUE_SERIAL)
    }()
    
    public lazy var previewLayer: AVCaptureVideoPreviewLayer! = {
        let layer =  AVCaptureVideoPreviewLayer(session: self.session)
        layer.videoGravity = AVLayerVideoGravityResizeAspectFill
        return layer
    }()
    
    private var _sessionPresset: CameraSessionPreset = .InputPriority
    public var sessionPresset: CameraSessionPreset!{
        get {
            return self._sessionPresset
        }
        set {
            if self.session.canSetSessionPreset(newValue.foundationPreset()) {
                self._sessionPresset = newValue
                self.session.sessionPreset = self._sessionPresset.foundationPreset()
            }
            else {
                fatalError("[CameraManager] session presset : [\(newValue.foundationPreset())] uncompatible with the current device")
            }
        }
    }
    
    /// camera set to capture
    var cameraIsReady: Bool = false
    
    
    // MARK: - camera functions(public)
    
    
    public func startWithDevicePosition(devicePosition:AVCaptureDevicePosition )->Bool
    {
        
        if captureDevice.currentPosition != devicePosition {
            captureDevice.changeCurrentDevice(devicePosition)
        
            // Obtain input port for camera device
            do{
                input = try AVCaptureDeviceInput(device: captureDevice.currentDevice)
            }
            catch let error{
                print(error)
            }
        
        
            // Configure input port for captureSession
            configureInputCamera(self.session, device: captureDevice.currentDevice!)
        }
        
        // (4) Configure output port for captureSession
        //addVideoDataOutput()
        
        // (5) Start captureSession running
        startCamera()
        
        //cameraIsReady = true
        return true
    }

    
    ///Don't need
    public func cameraWithPosition(position:AVCaptureDevicePosition)->AVCaptureDevice?{
        
        let devices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        for  device in devices  {
            let captureDevice = device as! AVCaptureDevice
            if (captureDevice.position == position) {
                return captureDevice
            }
        }
        return nil
    }
    
    public func changeCurrentDevice(position: AVCaptureDevicePosition) {
        self.captureDevice.changeCurrentDevice(position)
        self.configureInputDevice()
    }
    
    

    
    // MARK: - Capture image
    
   /*func capturePhoto(blockCompletion: blockCompletionCapturePhoto) {
        let connectionVideo = self.stillCameraOutput.connectionWithMediaType(AVMediaTypeVideo)
        connectionVideo.videoOrientation = AVCaptureVideoOrientation.orientationFromUIDeviceOrientation(UIDevice.currentDevice().orientation)
        
        self.stillCameraOutput.captureStillImageAsynchronouslyFromConnection(connectionVideo) { (sampleBuffer: CMSampleBuffer!, err: NSError!) -> Void in
            if let err = err {
                blockCompletion(image: nil, error: err)
            }
            else {
                if let sampleBuffer = sampleBuffer, let dataImage = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer) {
                    let image = UIImage(data: dataImage)
                    blockCompletion(image: image, error: nil)
                }
                else {
                    blockCompletion(image: nil, error: nil)
                }
            }
        }
    }*/

    
    // MARK: - Orientation handle
    private func handleDeviceOrientation() {
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        NSNotificationCenter.defaultCenter().addObserverForName(UIDeviceOrientationDidChangeNotification, object: nil, queue: NSOperationQueue.mainQueue()) { (_) -> Void in
            self.previewLayer.connection.videoOrientation = self.currentVideoOrientation()// AVCaptureVideoOrientation.orientationFromUIDeviceOrientation(UIDevice.currentDevice().orientation)
        }
    }

    
    // MARK: - AVCaptureVideoDataOutputSampleBufferDelegate
    
    public func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!)
    {
        // (1) Convert CMSampleBufferRef to CVImageBufferRef
        let imageBuffer: CVImageBufferRef! = CMSampleBufferGetImageBuffer(sampleBuffer)
        
        // (2) Lock pixel buffer
        CVPixelBufferLockBaseAddress(imageBuffer,  0) // or kCVPixelBufferLock_ReadOnly
        
        // (3) Construct VideoFrame struct
        //let frame = CameraUtil.imageBufferToVideoFrame(imageBuffer)
        
        // (4) Dispatch VideoFrame to VideoSource delegate
        delegate?.frameReady!(imageBuffer)
        
        // (5) Unlock pixel buffer
        CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
        
    }
    
    
    // MARK: - Lifecycle
    
    override init(){
        super.init();
        session.beginConfiguration ()
        setupSession()
        session.commitConfiguration()
    }
    
    deinit{
        session.stopRunning()
    }
    
    // MARK: - start/stop camera
    func startCamera() {
        dispatch_async(self.sessionQueue) {
            self.session.startRunning()
        }
    }
    
    func stopCamera() {
        dispatch_async(self.sessionQueue) {
            self.session.stopRunning()
        }
    }
    
    // MARK: - camera functions(private)
    
    func addVideoDataOutput()
    {
        // (1) Instantiate a new video data output object
        videoOutput = AVCaptureVideoDataOutput()
        
        // Frame that has delayed are ignored
        videoOutput.alwaysDiscardsLateVideoFrames = true;
        
        // (2) The sample buffer delegate requires a serial dispatch queue
        //let queue: dispatch_queue_t = dispatch_queue_create("myqueue",  DISPATCH_QUEUE_SERIAL) // or nil?
        videoOutput.setSampleBufferDelegate(self, queue: sessionQueue)
        
        // (3) Define the pixel format for the video data output
        videoOutput.videoSettings = [ kCVPixelBufferPixelFormatTypeKey: Int(kCVPixelFormatType_32BGRA) ]
        
        for connection in videoOutput.connections {
            if let conn = connection as? AVCaptureConnection {
                if conn.supportsVideoOrientation {
                    conn.videoOrientation = currentVideoOrientation()//AVCaptureVideoOrientation.orientationFromUIDeviceOrientation(UIDevice.currentDevice().orientation)
                }
            }
        }
        
        // (4) Configure the output port on the captureSession property
        session.addOutput(videoOutput)
        
    }
    
    func configureInputCamera(session: AVCaptureSession, device: AVCaptureDevice){
        do{
            let possibleCameraInput: AnyObject? = try AVCaptureDeviceInput(device: device)
            if let cameraInput = possibleCameraInput as? AVCaptureDeviceInput {
                if let currentDeviceInput = self.input {
                    session.removeInput(currentDeviceInput)
                }
                self.input = cameraInput
                if session.canAddInput(self.input) {
                    session.addInput(self.input)
                }
                else {
                
                    print("Unable to add camera")
                }
            }
        }
        catch let error{
            print(error)
        }
    }
    
    // MARK: - PreviewLayer
    
    public func configurePreviewLayer(view:UIView)
    {
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        let bounds:CGRect = view.layer.bounds
        //previewLayer.anchorPoint = CGPointZero
        previewLayer.bounds = bounds
        previewLayer.position = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds))
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer.connection?.videoOrientation = currentVideoOrientation()//AVCaptureVideoOrientation.orientationFromUIDeviceOrientation(UIDevice.currentDevice().orientation)
        view.layer.addSublayer(previewLayer)
    }
    
    public func updatePreviewLayer(view:UIView){
        let bounds:CGRect = view.layer.bounds
        previewLayer?.bounds = bounds
        previewLayer?.position = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds))
       
    }
    
    private func currentVideoOrientation() -> AVCaptureVideoOrientation {
        switch UIDevice.currentDevice().orientation {
        case .Portrait:
            return .Portrait
        case .LandscapeLeft:
            return .LandscapeRight
        case .LandscapeRight:
            return .LandscapeLeft
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        default:
            return .Portrait
        }
    }
    
    
    // MARK: - Setup
    private func setupSession() {
        dispatch_async(self.sessionQueue) { () -> Void in
            self.configureInputDevice()
            self.configureOutputDevice()
            self.handleDeviceOrientation()
        }
    }
    
    private func configureInputDevice() {
        self.sessionPresset = CameraSessionPreset.Res640x480
        let device: AVCaptureDevice = captureDevice.currentDevice!
        do {
            let input = try AVCaptureDeviceInput(device: device)
            if self.session.canAddInput(input) {
                self.session.addInput(input)
            }
        } catch {
            print(error)
        }
    }
    
    private func configureOutputDevice() {
        
        addVideoDataOutput()
        
    }

    
}

