//
//  ViewController.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 06/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK:Variables
    
    //button to open clide-out menu
    @IBOutlet weak var openMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var label: UILabel!
    
    var varView = Int()
    
    //MARK: System functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        customizeSlideOutMenu()
        customizeMenuButton()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: UI Customization
    func customizeSlideOutMenu()
    {
        // stop bouncing
        self.revealViewController().rearViewRevealOverdraw = 0.0;
        self.revealViewController().rearViewRevealWidth = 200.0;
        self.revealViewController().bounceBackOnOverdraw = false;// If YES the controller will bounce to the Left position when dragging further than 'rearViewRevealWidth' (default YES)
    }
    
    func customizeMenuButton()
    {
        openMenuButton.target = self.revealViewController()
        openMenuButton.action = Selector("revealToggle:")
    }


}

