//
//  EAGLView.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 25/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import Foundation


class Renderer:NSObject {
    
    private let openGLContext: EAGLContext
    private let coreImageContext: CIContext
    
    override init() {
        openGLContext = EAGLContext(API: .OpenGLES2)
        coreImageContext = CIContext(EAGLContext: openGLContext)
    }
    
  
}