//
//  Calibration.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 06/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import Foundation

// class for augmented reality view controller

class AugmentedRealityVC: UIViewController {
    
    override func viewDidLoad() {
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    
}
