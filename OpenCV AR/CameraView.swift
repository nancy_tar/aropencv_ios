//
//  CameraView.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 26/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import Foundation

class CameraView:UIView{
    
    var renderLayer: CALayer!
    var customPreviewLayer: CALayer!
    //var previewLayer:AVCaptureVideoPreviewLayer!
    var ciImage: CIImage!
    var filter: CIFilter!
    lazy var context: CIContext = {
        let eaglContext = EAGLContext(API: EAGLRenderingAPI.OpenGLES2)
        let options = [kCIContextWorkingColorSpace : NSNull()]
        return CIContext(EAGLContext: eaglContext, options: options)
    }()
    
    
}