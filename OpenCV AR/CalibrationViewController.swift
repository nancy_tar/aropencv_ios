//
//  CalibrationViewController.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 09/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import UIKit
import AVFoundation

class CalibrationViewController:UIViewController, CameraManagerDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var slideMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var cameraView: UIImageView!

    // collection of captured images for calibration
    var imageCollection:[UIImage] = []
    var opencv:OpenCVWrapper!
    // MARK: - Camera variables
    var renderLayer: CALayer!
    var customPreviewLayer: CALayer!
    var previewLayer:AVCaptureVideoPreviewLayer!
    var ciImage: CIImage!
    var filter: CIFilter!
    lazy var context: CIContext = {
        let eaglContext = EAGLContext(API: EAGLRenderingAPI.OpenGLES2)
        let options = [kCIContextWorkingColorSpace : NSNull()]
        return CIContext(EAGLContext: eaglContext, options: options)
    }()
    var camera :CameraManager!

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        customizeSlideOutMenu()
        customizeMenuButton()
        camera = CameraManager();
        opencv = OpenCVWrapper();
        // Prepare the camera
        
        camera.delegate = self;
        camera.startWithDevicePosition(AVCaptureDevicePosition.Back)
        //camera.configurePreviewLayer(cameraView)
        setPreviewLayer()
      
    }

    override func viewWillAppear(animated: Bool) {
    
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLayoutSubviews(){
        //update the previewLayer's position and bounds (and possibly others) every time the subviews' layout changes.
       updatePreviewLayer(cameraView)
        
     
        
    }
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        
        /*let oldHeight = cameraView.bounds.height
        let oldWidth = cameraView.bounds.width
        
        print("About to rotate: \(oldWidth) x \(oldHeight)")
        
        //        :C
        customPreviewLayer.anchorPoint = CGPointZero
        customPreviewLayer.bounds = CGRectMake(0, 0, oldHeight, oldWidth)*/
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        //camera.updatePreviewLayer(cameraView)
    }

    
    // MARK: - VideoSourseDelegate
    
    func frameReady(imageBuffer: CVImageBufferRef) {
        
        dispatch_sync(dispatch_get_main_queue(), { () -> Void in
            
            var outputImage = CIImage(CVPixelBuffer: imageBuffer)
            
            let orientation = UIDevice.currentDevice().orientation
            var t: CGAffineTransform!
            if orientation == UIDeviceOrientation.Portrait {
                t = CGAffineTransformMakeRotation(CGFloat(-M_PI / 2.0))
            } else if orientation == UIDeviceOrientation.PortraitUpsideDown {
                t = CGAffineTransformMakeRotation(CGFloat(M_PI / 2.0))
            } else if (orientation == UIDeviceOrientation.LandscapeRight) {
                t = CGAffineTransformMakeRotation(CGFloat(M_PI))
            } else {
                t = CGAffineTransformMakeRotation(0)
            }
            outputImage = outputImage.imageByApplyingTransform(t)
            
            let cgImage = self.context.createCGImage(outputImage, fromRect: outputImage.extent)
            //var frame = CameraUtil.imageBufferToVideoFrame(imageBuffer)
            //var cgImage = CameraUtil.videoFrameToCGImage(frame)
            //self.ciImage = outputImage
            
            //dispatch_sync(dispatch_get_main_queue(), {
            var red = UIColor(red: 100.0/255.0, green: 130.0/255.0, blue: 230.0/255.0, alpha: 1.0)
           
            //self.camera.previewLayer.backgroundColor = red.CGColor
            self.customPreviewLayer.contents = cgImage
            
            let bounds:CGRect = self.cameraView.bounds
            
            print("width:\(bounds.width) height:\(bounds.height)")
            //})

            
            //self.previewLayer.contents = cgimage
        })
    }

    // MARK: - UI Customization
    func customizeSlideOutMenu()
    {
        // stop bouncing
        self.revealViewController().rearViewRevealOverdraw = 0.0;
        self.revealViewController().rearViewRevealWidth = 200.0;
        self.revealViewController().bounceBackOnOverdraw = false;// If YES the controller will bounce to the Left position when dragging further than 'rearViewRevealWidth' (default YES)
    }
    
    func customizeMenuButton()
    {
        slideMenuButton.target = self.revealViewController()
        slideMenuButton.action = Selector("revealToggle:")
    }

    // MARK: - PreviewLayer
    func setPreviewLayer(){
        
        customPreviewLayer = CALayer()
        let bounds:CGRect = cameraView.bounds
        customPreviewLayer.anchorPoint = CGPointZero
        customPreviewLayer.contentsGravity = kCAGravityResizeAspectFill
        //customPreviewLayer.position = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds))
        //customPreviewLayer.bounds = cameraView.bounds
        customPreviewLayer.bounds = CGRectMake(0, 0, bounds.width, bounds.height)
        print("width:\(bounds.width) height:\(bounds.height)")
        self.cameraView.layer.insertSublayer(customPreviewLayer, atIndex: 0)
        //updatePreviewLayer(cameraView)
    
    
        
    }
    
    func updatePreviewLayer(view:UIView){
        let bounds:CGRect = view.layer.bounds
        customPreviewLayer.anchorPoint = CGPointZero
        customPreviewLayer.bounds = CGRectMake(0, 0, bounds.width, bounds.height)
        //customPreviewLayer?.position = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds))
        
    }
    
    func rotatePreviewLayer(){
    
        
    }
    

}
