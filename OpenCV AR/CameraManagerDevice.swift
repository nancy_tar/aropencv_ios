//
//  CameraManagerDevice.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 20/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import Foundation



class CameraManagerDevice {
    
    private var backCameraDevice: AVCaptureDevice!
    private var frontCameraDevice: AVCaptureDevice!
    var currentDevice: AVCaptureDevice?
    var currentPosition: AVCaptureDevicePosition = .Unspecified
    
    
    func changeCurrentDevice(position: AVCaptureDevicePosition) {
        self.currentPosition = position
        switch position {
            case .Back: self.currentDevice = self.backCameraDevice
            case .Front: self.currentDevice = self.frontCameraDevice
            case .Unspecified: self.currentDevice = nil
        }
    }
    
    private func configureDeviceCamera() {
        let availableCameraDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        for device in availableCameraDevices as! [AVCaptureDevice] {
            if device.position == .Back {
                self.backCameraDevice = device
            }
            else if device.position == .Front {
                self.frontCameraDevice = device
            }
        }
    }
    
    
    init() {
        self.configureDeviceCamera()
        self.changeCurrentDevice(.Back)
    }
}
