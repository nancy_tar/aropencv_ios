//
//  CVImageUtil.m
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 15/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//



#import "ImageManager.h"
#import <opencv2/opencv.hpp>
#import "opencv2/imgcodecs/ios.h"

@interface ImageManager()
@property (nonatomic)CVImageBufferRef imageBuffer;
@property (nonatomic)CGColorSpaceRef csrColorSpace;
@property (nonatomic)uint8_t *baseAddress;
@property (nonatomic)size_t sztBytesPerRow;
@property (nonatomic)size_t sztWidth;
@property (nonatomic)size_t sztHeight;
@property (nonatomic)CGContextRef cnrContext;
@property (nonatomic)CGImageRef imrImage;
@end

@implementation ImageManager


+ (cv::Mat ) createMatFromBuffer:(CMSampleBufferRef) sampleBuffer{

    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    /*Lock the image buffer*/
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    
    /*Get information about the image*/
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    //size_t stride = CVPixelBufferGetBytesPerRow(imageBuffer);
    //NSLog(@"Frame captured: %lu x %lu", width,height);
    
    cv::Mat frame(height, width, CV_8UC4, (void*)baseAddress);
    
    /*We unlock the  image buffer*/
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    return frame;
}

+ (void)convertYUVSampleBuffer:(CMSampleBufferRef)buffer toGrayscaleMat:(cv::Mat &)mat {
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(buffer);
    
    // lock the buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // get the address to the image data
    void *imageBufferAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
    
    // get image properties
    int w = (int)CVPixelBufferGetWidth(imageBuffer);
    int h = (int)CVPixelBufferGetHeight(imageBuffer);
    
    // create the cv mat
    mat.create(h, w, CV_8UC1);              // 8 bit unsigned chars for grayscale data
    memcpy(mat.data, imageBufferAddress, w * h);    // the first plane contains the grayscale data
    // therefore we use <imgBufAddr> as source
    
    // unlock again
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
}

+ (void)convertYUVSampleBuffer:(CMSampleBufferRef)buffer toBGR_Mat:(cv::Mat &)mat {
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(buffer);
    
    // lock the buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // get the address to the image data
    void *imageBufferAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
    
    // get image properties
    int w = (int)CVPixelBufferGetWidth(imageBuffer);
    int h = (int)CVPixelBufferGetHeight(imageBuffer);
    
    // create the cv mat
    mat.create(h, w, CV_8UC3);              // 8 bit unsigned chars for grayscale data
    memcpy(mat.data, imageBufferAddress, w * h);    // the first plane contains the grayscale data
    // therefore we use <imgBufAddr> as source
    
    // unlock again
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
}

+(void)convertYUVSampleBuffer:(CMSampleBufferRef)buffer toBGRA_Mat:(cv::Mat &)mat {
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(buffer);
    
    // lock the buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // get the address to the image data
    void *imageBufferAddress = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
    
    // get image properties
    int w = (int)CVPixelBufferGetWidth(imageBuffer);
    int h = (int)CVPixelBufferGetHeight(imageBuffer);
    
    // create the cv mat
    mat.create(h, w, CV_8UC4);              // 8 bit unsigned chars for grayscale data
    
    
    memcpy(mat.data, imageBufferAddress, w * h);    // the first plane contains the grayscale data
    // therefore we use <imgBufAddr> as source
    
    // unlock again
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
}


+ (cv::Mat ) createMatFromVideoFrame:(VideoFrame) frame{

    //cv::Mat mat(frame.height, frame.width, CV_8UC4, (void*)frame.data);
    
    // create the cv mat
    
    
    cv::Mat bgraMat(frame.height, frame.width, CV_8UC4, frame.rawPixelData, frame.bytesPerRow);
    //mat.create(frame.height, frame.width, CV_8UC4);              // 8 bit unsigned chars for grayscale data
    //memcpy(mat.data, frame.data, frame.height*frame.width);    // the first plane contains the grayscale data
    // therefore we use <imgBufAddr> as source

    return bgraMat;
}



@end