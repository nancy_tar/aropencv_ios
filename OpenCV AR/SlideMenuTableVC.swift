//
//  BackTableVC.swift
//  OpenCV AR
//
//  Created by Anastasia Tarasova on 06/02/16.
//  Copyright © 2016 Anastasia Tarasova. All rights reserved.
//

import Foundation

class SlideMenuTableVC: UITableViewController {
    
    var TableArray = [String]()
    
    override func viewDidLoad() {
        TableArray = ["Video Feed","Camera Calibration","Augmented Reality"]
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(TableArray[indexPath.row], forIndexPath: indexPath) as UITableViewCell
        
        cell.textLabel!.text = TableArray[indexPath.row]
        return cell
        
    }
    
}
